import cv2
import numpy
import numpy as np

frame_width = 640
frame_height = 480
cap = cv2.VideoCapture(0)
cap.set(3,frame_width)
cap.set(4,frame_height)
cap.set(10,150)

def PreProcess(image):
    Gray_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    Blur_image = cv2.GaussianBlur(Gray_image,(7,7),1)
    Canny_image = cv2.Canny(Blur_image,100,100)
    kernal = numpy.ones((5,5))
    Dialated_image = cv2.dilate(Canny_image,kernal,iterations=2)
    Eroded_image = cv2.erode(Dialated_image,kernal,iterations=1)
    return Eroded_image

def GetContour(image):
    biggest = numpy.array([])
    maxArea = 0
    contours, hierarchy = cv2.findContours(image,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    for cnt in contours:
        area = cv2.contourArea(cnt)
        if area > 3000:
            # cv2.drawContours(Countour_image, cnt, -1, (255,0,0), 3)
            perimeter = cv2.arcLength(cnt,True)
            approx = cv2.approxPolyDP(cnt,0.02*perimeter,True)
            if area > maxArea and len(approx) == 4:
                biggest = approx
                maxArea = area
    cv2.drawContours(Countour_image, biggest, -1, (255, 0, 0), 20)
    return biggest

# def stackImages(imgArray,scale,lables=[]):
#     rows = len(imgArray)
#     cols = len(imgArray[0])
#     rowsAvailable = isinstance(imgArray[0], list)
#     width = imgArray[0][0].shape[1]
#     height = imgArray[0][0].shape[0]
#     if rowsAvailable:
#         for x in range ( 0, rows):
#             for y in range(0, cols):
#                 imgArray[x][y] = cv2.resize(imgArray[x][y], (0, 0), None, scale, scale)
#                 if len(imgArray[x][y].shape) == 2: imgArray[x][y]= cv2.cvtColor( imgArray[x][y], cv2.COLOR_GRAY2BGR)
#         imageBlank = np.zeros((height, width, 3), np.uint8)
#         hor = [imageBlank]*rows
#         hor_con = [imageBlank]*rows
#         for x in range(0, rows):
#             hor[x] = np.hstack(imgArray[x])
#             hor_con[x] = np.concatenate(imgArray[x])
#         ver = np.vstack(hor)
#         ver_con = np.concatenate(hor)
#     else:
#         for x in range(0, rows):
#             imgArray[x] = cv2.resize(imgArray[x], (0, 0), None, scale, scale)
#             if len(imgArray[x].shape) == 2: imgArray[x] = cv2.cvtColor(imgArray[x], cv2.COLOR_GRAY2BGR)
#         hor= np.hstack(imgArray)
#         hor_con= np.concatenate(imgArray)
#         ver = hor
#     if len(lables) != 0:
#         eachImgWidth= int(ver.shape[1] / cols)
#         eachImgHeight = int(ver.shape[0] / rows)
#         print(eachImgHeight)
#         for d in range(0, rows):
#             for c in range (0,cols):
#                 cv2.rectangle(ver,(c*eachImgWidth,eachImgHeight*d),(c*eachImgWidth+len(lables[d])*13+27,30+eachImgHeight*d),(255,255,255),cv2.FILLED)
#                 # cv2.putText(ver,lables[d],(eachImgWidth*c+10,eachImgHeight*d+20),cv2.FONT_HERSHEY_COMPLEX,0.7,(255,0,255),2)
#     return ver

while (True):
    success, img = cap.read()
    Threshold_image = PreProcess(img)
    Countour_image = img.copy()
    biggest = GetContour(Threshold_image)
    print(biggest)
    # stack_image = cv2.vconcat([Threshold_image,Countour_image])
    # stack =([img,Threshold_image],[img,Countour_image])
    # lables =[["original","Threshold_image"],["img","Countour_image"]]
    # stack_image = stackImages(stack,0.75,lables)

    # cv2.imshow("original image", img)
    cv2.imshow("Threshold_image", Threshold_image)
    cv2.imshow("Countour_image", Countour_image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break