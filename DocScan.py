import cv2
import numpy

frame_width = 640
frame_height = 480
cap = cv2.VideoCapture(0)
cap.set(3,frame_width)
cap.set(4,frame_height)
cap.set(10,150)

def PreProcess(image):
    Gray_image = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    Blur_image = cv2.GaussianBlur(Gray_image,(5,5),1)
    Canny_image = cv2.Canny(Blur_image,50,50)
    kernal = numpy.ones((5,5))
    Dialated_image = cv2.dilate(Canny_image,kernal,iterations=2)
    Eroded_image = cv2.erode(Dialated_image,kernal,iterations=1)
    return Eroded_image


def GetContour(image):
    biggest = numpy.array([])
    maxArea = 0
    contours, hierarchy = cv2.findContours(image,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
    for cnt in contours:
        area = cv2.contourArea(cnt)
        if area > 3000:
            # cv2.drawContours(Countour_image, cnt, -1, (255,0,0), 3)
            perimeter = cv2.arcLength(cnt,True)
            approx = cv2.approxPolyDP(cnt,0.02*perimeter,True)
            if area > maxArea and len(approx) == 4:
                biggest = approx
                maxArea = area
    cv2.drawContours(Countour_image, biggest, -1, (255, 0, 0), 20)
    return biggest

def reorder(mypoints):
    mypoints = mypoints.reshape((4,2))
    mypointsNew = numpy.zeros((4,1,2),np.int32)
    add = mypoints.sum(1)
    diff = numpy.diff(mypoints,axis=1)
    mypointsNew[0] = mypoints[numpy.argmin(add)]
    mypointsNew[3] = mypoints[numpy.argmax(add)]
    mypointsNew[1] = mypoints[numpy.argmin(diff)]
    mypointsNew[2] = mypoints[numpy.argmax(diff)]
    return mypointsNew

def GetWarp(image,biggest):
    biggest = reorder(biggest)
    pts1=numpy.float32(biggest)
    pts2=np.float32([[0,0],[frame_width,0],[0,frame_height],[frame_width,frame_height]])
    matrix = cv2.getPerspectiveTransform(pts1,pts2)
    imgOutput = cv2.warpPerspective(img, matrix, (frame_width, frame_height))

    return imgOutput



while True:
    success, img = cap.read()
    img = cv2.resize(img,(frame_width,frame_height))
    Countour_image = img.copy()

    Threshold_image = PreProcess(img)
    biggest = GetContour(Threshold_image)
    print(biggest)
    if biggest.size !=0:
        Warped_image=GetWarp(img,biggest)
    else:
        Warped_image=img

    cv2.imshow("Video",Warped_image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break